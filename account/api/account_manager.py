from account.models import Account
from .token_manager import TokenManager
from blog import settings


class AccountManager:
    classmethod

    def getAccountByToken(token: str):
        """Get account by token
        Args:
        * token (str)
        
        return:
        * Account
        """

        try:

            # decode token and extract id
            id = TokenManager.decode_user_token(token)['id']

            # get user with id
            account = Account.objects.get(id=id)
            return account

        except Exception as e:
            if settings.DEBUG:
                print(e)
            # if there is a problem, return value will be 'None' 
            return None
