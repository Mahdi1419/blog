from api_app.response import ApiResponse


def authRequired(func):
    def wrapper(view, request, *args, **kwargs):
        if request.auth_user is None:
            return ApiResponse({"message": "Auth required (or token invalid)"}, status=403)
        response = func(view, request, *args, **kwargs)
        return response

    return wrapper


def adminRequired(func):
    def wrapper(view, request, *args, **kwargs):
        if not request.auth_user.is_admin:
            return ApiResponse({"message": "Only admins access to this url"}, status=403)
        response = func(view, request, *args, **kwargs)
        return response

    return wrapper


def activeRequired(func):
    def wrapper(view, request, *args, **kwargs):
        if not request.auth_user.is_active:
            return ApiResponse({"message": "Account dose not exist"}, status=403)
        response = func(view, request, *args, **kwargs)
        return response

    return wrapper
