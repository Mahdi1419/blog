from django.urls import path, re_path

#import views
from .views import AuthView, PanelView, VerifyCodeView


app_name = 'account'

urlpatterns = [
    path('auth/', AuthView.as_view(), name='auth'), 
    re_path(r'^auth/verifycode/(?P<mode>(login)|(delete))/$', VerifyCodeView.as_view(), name='verify')
]

