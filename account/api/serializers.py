
from api_app.Serializer import Serializer
from ..models import Account

class LoginSerializer(Serializer):
    token = Serializer.RelationField(fields=['token'], field_name='get_token')

    class Meta:
        model = Account
        fields = ['get_token']
