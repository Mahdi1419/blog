
from django.db.models.signals import post_save, pre_delete, post_delete, pre_save
from django.dispatch import receiver
from .api.token_manager import TokenManager
from .models import Token, Account

@receiver(post_save, sender=Account)
def user_setting_create(sender, instance, created, **kwargs):
    if created:
        try:
            token = TokenManager.create_user_token(instance)
            Token.objects.create(account=instance, token=token)
        except Exception as e:
            instance.delete()
