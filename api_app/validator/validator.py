from django.core.exceptions import ValidationError
from ..Serializer import WarningHandler


class ValidateData:
    """return validate data, errors, model instance and original data"""

    def __init__(self, data, validate_data, messages) -> None:
        self.__data = data
        self.__validate_data = validate_data
        self.__messages = messages

    def get_original_data(self):
        return self.__data

    def get_validate_data(self):
        return self.__validate_data

    def get_messages(self):
        return self.__messages if bool(self.__messages) else None

    def is_valid(self):
        return False if bool(self.__messages) else True


# message handler
warning = WarningHandler()


class Validator:

    def __init__(self, data: dict, fields=None, validators=None, extra_data=None) -> None:
        """initialize Creator

        Args:
            data (dict)
            instance (instance if Model, optional): If you want to update one object, instance Can not be null. Defaults to None.
            extra_data (dict, optional): Extra data for use in create and update function. Defaults to None.
        """

    def __new__(cls, *args, **kwargs):
        if cls.__get_attrs(cls, 'Meta') is None:
            raise Exception('Serializer must have a meta class ')

        __fields: list = kwargs.get('fields') or cls.__get_attrs(cls.Meta, 'fields')
        __extra_data: list = kwargs.get('extra_data')
        __validators = cls.__get_attrs(cls, 'validators')

        __data: dict = kwargs.get('data')
        __messages = dict()

        # fields can not be None
        if __fields is None:
            raise ValueError("%s must have fields" % cls.__name__)

        __validate_data = dict()

        for field, field_type in __fields:

            # checking field is in data keys
            if field not in __data:
                raise KeyError("%s not in %s" % (field, list(__data.keys())))

            try:
                # clean and validate value
                value = field_type.clean(__data[field])
                # add validated value to validates data
                __validate_data.update({field: value})

            except ValidationError as e:
                # if value not valid add message to messages
                __messages.update({field: e.messages})

        # custom validators
        if not bool(__messages):
            if __validators is not None:
                for validator in __validators:
                    try:
                        validator(__validate_data)
                    except ValidationError as e:
                        if __messages.get('errors') is not None:
                            __messages.get('errors').append(e.message)
                        else:
                            __messages.update({'errors': e.messages})

        warning.show_warnings()
        return ValidateData(__data, __validate_data, __messages)

    @classmethod
    def validate(cls, value, extra_ata):
        pass

    @classmethod
    def __get_attrs(cls, obj, name):
        """get attrs from objects

        Args:
            obj (class)
            name (str)

        Returns:
            [attr, None]
        """
        if hasattr(obj, name):
            return getattr(obj, name)
        else:
            return None
