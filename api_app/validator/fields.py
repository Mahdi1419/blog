from django.forms import *
from django.contrib.postgres.forms import *


class BooleanField(NullBooleanField):
    """Custom Boolean Field"""
    TRUE_VALUES = {
        't', 'T',
        'y', 'Y', 'yes', 'Yes', 'YES',
        'true', 'True', 'TRUE',
        'on', 'On', 'ON',
        '1', 1,
        True
    }

    FALSE_VALUES = {
        'f', 'F',
        'n', 'N', 'no', 'No', 'NO',
        'false', 'False', 'FALSE',
        'off', 'Off', 'OFF',
        '0', 0, 0.0,
        'Null', 'null',
        False, None
    }

    default_error_messages = {
        'invalid': 'Enter a boolean'
    }

    def to_python(self, value):
        """add some choice to boolean field"""
        if value in self.TRUE_VALUES:
            value = True
        elif value in self.FALSE_VALUES:
            value = False

        else:
            raise ValidationError(self.error_messages['invalid'], 'invalid')

        return super().to_python(value)

    pass


class ArrayField(SimpleArrayField):
    pass
