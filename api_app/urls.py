from django.urls import path, include


app_name = 'api_app'

urlpatterns = [
    path('account/', include('account.api.urls')),
    path('blogger/', include('blogger.api.urls')),
    path('post/', include('post.api.urls'))
]

