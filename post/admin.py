from django.contrib import admin

# models
from .models import Post, PostSetting, Comment, Like
# Register your models here.



admin.site.register((Post, PostSetting, Comment, Like))