from api_app.response import ApiResponse


class PostStaticResponse:
    """
    static json responses
    """

    PostNotFound = ApiResponse({
        "message": 'post not found'
    }, status=404)

    CommentLimited = ApiResponse({
        "message": 'comments limited',
    }, status=429)