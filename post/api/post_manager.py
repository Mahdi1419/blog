from api_app.response import ApiResponse
from api_app.utils import checkUUid
from post.models import Post


class PostManager:
    classmethod

    def get_post_with_uid(uid:str):
        # uuid check
        uid_check = checkUUid(uid, 4)
        if not uid_check:
            return None

        # get post
        post = Post.is_exist({"uid": uid})
        if not post:
            return None

        return post

