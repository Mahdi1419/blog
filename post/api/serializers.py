from api_app.Serializer import Serializer
from post.models import Post, Like, Comment


class PostSerializer(Serializer):
    comment = Serializer.RelationField(except_fields=['id', 'post_id'])
    like = Serializer.RelationField()
    postsetting = Serializer.RelationField()
    getLastComments = Serializer.CustomField('get_last_comments', field_name='comments')
    getLastLikes = Serializer.CustomField('get_last_likes', field_name='likes')

    class Meta:
        model = Post
        fields = '__all__'
        except_fields = ['blogger_id', 'id', 'postsetting']
        filters = ['!archive']

    def get_last_comments(self, obj, instance, serializer):
        if not serializer.postsetting.comment:
            return False
        else:
            return CommentSerializer(instance=serializer.comment, many=True)['data'][:3]

    def get_last_likes(self, obj, instance, serializer):
        likes = LikeSerializer(instance=serializer.like, many=True)['data'][:3]
        like_list = []

        for like in likes:
            like_list.append(like.get('like').get('username'))

        return like_list


class LikeSerializer(Serializer):
    liker = Serializer.RelationField(fields=['username'])
    post = Serializer.RelationField(fields=['uid'])
    like_detail = Serializer.CustomField('get_like_details', field_name='like')

    class Meta:
        model = Like
        fields = ['like']

    def get_like_details(self, obj, instance, serializer):
        username = obj.liker.get('username')
        post = obj.post.get('uid')

        return {
            "username": "@%s" % username,
            "post": post
        }



class CommentSerializer(Serializer):
    sender = Serializer.RelationField(fields=['username'])
    post = Serializer.RelationField(fields=['uid'])

    comment_detail = Serializer.CustomField('get_comment_detail')

    class Meta:
        model = Comment
        fields = ['uid', 'comment_detail']

    def get_comment_detail(self, obj, instance, serializer) -> any:
        return {
            "sender": obj.sender.get('username'),
            "post": obj.post.get('uid'),
            "mentions": obj.mentions,
            "hashtags": obj.hashtags
        }
