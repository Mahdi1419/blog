# django
import re
from django.views.generic.base import View
from api_app.utils import checkUUid
from blog import settings

# serializers
from blogger.api.blogger_manager import BloggerManager
from blogger.api.response import BloggerStaticResponse
from post.api.serializers import PostSerializer, LikeSerializer, CommentSerializer

# models
from .response import PostStaticResponse
from ..models import Like, Post, Comment
from blogger.models import Blogger, Follow

# api tools
from api_app.response import ApiResponse, StaticResponse

# decorator
from account.api.decorators import authRequired, activeRequired
from api_app.decorators import parmsCheck

# validator
from api_app.validator.validator import ValidateData
from post.api.validators import PostValidator, CommentValidator
from api_app.validator.fields import ArrayField, BooleanField, CharField

# managers
from .post_manager import PostManager

# exceptions
from django.db.utils import IntegrityError


class PostView(View):
    """Get post data from database"""

    @authRequired
    @parmsCheck(needed_params=['pin', 'content', 'title', 'description'])
    def post(self, request):
        """create posts

        Returns:
            JsonResponse
        """

        # get auth blogger
        try:
            blogger = Blogger.objects.get(account=request.auth_user)

            # validate request data
            validate: ValidateData = PostValidator(data=request.data)

            # if data not valid
            if not validate.is_valid():
                return ApiResponse(validate.get_messages(), status=403)

            # create post instance
            post = Post(
                blogger=blogger,
                **validate.get_validate_data()
            )
            post.save()

            # post serialize
            serialize_data = PostSerializer(instance=post)

            # return serialize data
            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


class SinglePostView(View):
    """return single post data from database"""

    def get(self, request, uid):
        """get single post data"""
        try:

            uid_check = checkUUid(uid, 4)

            if not uid_check:
                return PostStaticResponse.PostNotFound

            post = Post.is_exist({"uid": uid})
            if not post:
                print('here')
                return PostStaticResponse.PostNotFound

            serialize_data = PostSerializer(instance=post)
            auth_user = request.auth_user
            post_public = post.postsetting.public
            page_privacy = post.blogger.bloggersetting.privacy

            if not post_public:
                return PostStaticResponse.PostNotFound

            if not page_privacy:
                return ApiResponse(serialize_data, status=200)

            if auth_user is None:
                return BloggerStaticResponse.PageIsPrivate

            post_owner = post.blogger
            blogger = Blogger.objects.get(account=auth_user)

            if post_owner == blogger:
                return ApiResponse(serialize_data, status=200)

            # check follow
            follow_obj = BloggerManager.check_follow(blogger, post_owner, 1)

            if not follow_obj:
                return BloggerStaticResponse.PageIsPrivate

            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    @authRequired
    def post(self, request, uid):

        try:
            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)
            uid_check = checkUUid(uid, 4)
            if not uid_check:
                return PostStaticResponse.PostNotFound

            # get post
            post_obj = Post.is_exist({"uid": uid, "blogger": auth_blogger})

            if not post_obj:
                return PostStaticResponse.PostNotFound

            # validate request data
            validate: ValidateData = PostValidator(data=request.data)

            if not validate.is_valid():
                return ApiResponse(validate.get_messages(), status=403)

            # update post    
            update = Post.custom_update(
                post_obj,
                data=validate.get_validate_data(),
                update_fields=['title', 'content', 'body', 'description', 'pin']
            )

            serialize_data = PostSerializer(instance=update)

            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


class LikeView(View):
    """Get like data from datavase"""
    @authRequired
    @activeRequired
    def get(self, request, uid):

        # get auth blogger
        auth_blogger = Blogger.objects.get(account=request.auth_user)

        # get post
        post = PostManager.get_post_with_uid(uid)
        if post is None or not post.postsetting.public:
            return PostStaticResponse.PostNotFound

        post_owner = post.blogger
        page_privacy = post_owner.bloggersetting.privacy

        if page_privacy:
            follow_obj = BloggerManager.check_follow(auth_blogger, post_owner, 1)
            if not follow_obj:
                return BloggerStaticResponse.PageIsPrivate

        # get all likes
        likes = Like.objects.filter(post=post)

        serialize_data = LikeSerializer(instance=likes, many=True, key='likes')
        serialize_data.update({"likes_count": likes.count()})

        return ApiResponse(serialize_data)

    @authRequired
    @activeRequired
    def post(self, request, uid):
        """Add like to posts"""
        try:

            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get post
            post = PostManager.get_post_with_uid(uid)
            if post is None or not post.postsetting.public:
                return PostStaticResponse.PostNotFound

            post_owner = post.blogger
            page_privacy = post_owner.bloggersetting.privacy

            if page_privacy:
                follow_obj = BloggerManager.check_follow(auth_blogger, post_owner, 1)
                if not follow_obj:
                    return BloggerStaticResponse.PageIsPrivate

            # create like object
            like_obj = Like(
                liker=auth_blogger,
                post=post
            )

            like_obj.save()

            return ApiResponse({"message": "post like successfully"}, status=201)

        except IntegrityError as e:
            # if like already exist
            if 'duplicate key' in str(e):
                return ApiResponse({"message": "you already liked this post"})

            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    @authRequired
    @activeRequired
    def delete(self, request, uid):
        """unlike posts"""
        try:
            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get post
            post = PostManager.get_post_with_uid(uid)
            if post is None or not post.postsetting.public:
                return PostStaticResponse.PostNotFound

            post_owner = post.blogger
            page_privacy = post_owner.bloggersetting.privacy

            if page_privacy:
                follow_obj = BloggerManager.check_follow(auth_blogger, post_owner, 1)
                if not follow_obj:
                    return BloggerStaticResponse.PageIsPrivate

            # get like object
            like_obj = Like.is_exist({
                "liker": auth_blogger,
                "post": post
            })

            if not like_obj:
                return ApiResponse({'message': 'You have not liked the post yet'}, status=404)

            # unlike post
            like_obj.delete()
            return ApiResponse({"message": "post like successfully"}, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError


class CommentView(View):
    """get comment data from database"""

    @authRequired
    @activeRequired
    def get(self, request, uid):
        """get comments data"""
        try:

            # get auth_blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get post
            post = PostManager.get_post_with_uid(uid)
            if post is None or not post.postsetting.public:
                return PostStaticResponse.PostNotFound

            # get post's blogger
            post_owner = post.blogger

            # get page privacy
            page_privacy = post_owner.bloggersetting.privacy

            if page_privacy:
                follow_obj = BloggerManager.check_follow(auth_blogger, post_owner, 1)
                if not follow_obj:
                    return BloggerStaticResponse.PageIsPrivate

            # get blogger comments setting
            page_privacy = post.blogger.bloggersetting.comments

            # get comments
            if not post.postsetting.comment or not page_privacy:
                return ApiResponse({"message": "comments limited"}, status=429)

            comments = Comment.objects.filter(
                post=post
            )

            serialize_data = CommentSerializer(instance=comments, many=True, key='comments')
            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError

    @authRequired
    @activeRequired
    @parmsCheck(needed_params=['text'])
    def post(self, request, uid):
        """create and return post comment"""

        try:

            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get post
            post = PostManager.get_post_with_uid(uid)
            if post is None or not post.postsetting.public:
                return PostStaticResponse.PostNotFound

            # get post owner
            post_owner = post.blogger

            # get page privacy
            page_privacy = post_owner.bloggersetting.privacy

            # get post comment mode (True/False)
            post_comment = post.postsetting.comment
            if page_privacy:
                follow_obj = BloggerManager.check_follow(auth_blogger, post_owner, 1)
                if not follow_obj:
                    return BloggerStaticResponse.PageIsPrivate

            # get blogger comments setting
            page_comments = post_owner.bloggersetting.comments

            if not post_comment or not page_comments:
                return ApiResponse({"message": "comments limited"}, status=429)

            # validate request data
            validator: ValidateData = CommentValidator(data=request.data)

            if not validator.is_valid():
                return ApiResponse(validator.get_messages(), status=403)

            # create comment object
            comment_obj = Comment(
                sender=auth_blogger,
                post=post,
                **validator.get_validate_data()
            )
            comment_obj.save()

            # get comment data
            serialize_data = CommentSerializer(instance=comment_obj, key='comment')

            # return serialize data
            return ApiResponse(serialize_data, status=201)

        except Exception as e:
            if settings.DEBUG:
                print(e)

            return StaticResponse.ServerError



