from django.urls import path, include
from .views import LikeView, PostView, SinglePostView, CommentView

app_name = 'post'

urlpatterns = [
    path('', PostView.as_view(), name='posts'),
    path('<str:uid>/', SinglePostView.as_view(), name='singles'),
    path('<str:uid>/like/', LikeView.as_view(), name='like'),
    path('<str:uid>/comment/', CommentView.as_view(), name='comment'),
]

