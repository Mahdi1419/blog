from django.core.exceptions import ValidationError
# from django.forms.fields import CharField, BooleanField, IntegerField
from api_app.validator.fields import *
from api_app.validator.validator import Validator
from blogger.models import Blogger
from ..models import Post


class PostValidator(Validator):

    def null_check(validate_data):
        title = validate_data.get('title')
        content = validate_data.get('content')
        description = validate_data.get('description')

        if title + content + description == '':
            raise ValidationError("title, content and description can not be empty together")
        pass

    class Meta:
        fields = [
            ('title', CharField(max_length=255, min_length=3, required=False)),
            ('description', CharField(required=False)),
            ('content', CharField(max_length=1024, required=False)),
            ('pin', BooleanField()),
        ]

    validators = [null_check]


class CommentValidator(Validator):
    class Meta:
        fields = [
            ('text', CharField(max_length=560, required=True))
        ]
