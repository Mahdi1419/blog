# Generated by Django 3.2.9 on 2021-11-27 09:53

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0007_alter_like_unique_together'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='hashtags',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=255, null=True), default=[], size=None),
            preserve_default=False,
        ),
    ]
