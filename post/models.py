from enum import unique
import uuid
from django.utils import timezone
# models
from django.db import models
from api_app.utils import SuperModel
from blogger.models import Blogger

# Fields
from django.contrib.postgres.fields import ArrayField


# Create your models here.


class Post(models.Model, SuperModel):
    """Blog Posts Model.
    
    Properies:
        - blogger (Blogger)
        - title (str)
        - descriptions (str)
        - content (str)
        - imagese (list of urls)
        - menstions (list)
        - hashtags (list)
        - pin (boolean)
        - is_super_post (boolean)
    """

    uid = models.UUIDField(default=uuid.uuid4)
    blogger = models.ForeignKey(Blogger, on_delete=models.CASCADE)

    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(max_length=560, null=True, blank=True)
    content = models.TextField(null=True, blank=True)

    images = ArrayField(models.URLField(), blank=True, null=True)

    mentions = ArrayField(models.CharField(max_length=255, null=True, blank=True), blank=True, null=True)
    hashtags = ArrayField(models.CharField(max_length=255, null=True, blank=True), blank=True, null=True)

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    likes_count = models.IntegerField(default=0)
    comments_count = models.IntegerField(default=0)
    views_count = models.IntegerField(default=0)

    pin = models.BooleanField(default=False)
    is_super_post = models.BooleanField(default=False)

    archive = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s" % (self.blogger, self.id)


class PostSetting(models.Model):
    """blog post settings

    Properties:
        - post (Post)
        - comment (boolean): Closed or open post comments
        - public (boolean): Public or private post
    """
    uid = models.UUIDField(default=uuid.uuid4)
    post = models.OneToOneField(Post, on_delete=models.CASCADE)
    comment = models.BooleanField(default=True)
    public = models.BooleanField(default=True)

    def __str__(self):
        return f'%s' % (self.post)


class Comment(models.Model):
    """Post comments model
    
    Properties:
        - sender (Blogger): Instance of Blogger model
        - post (Post): Instance of Post model
        - test (str): Comment text
    """
    uid = models.UUIDField(default=uuid.uuid4)
    sender = models.ForeignKey(to=Blogger, on_delete=models.CASCADE)
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE)
    text = models.TextField()
    mentions = ArrayField(models.CharField(max_length=255, null=True, blank=True), null=True, blank=True)
    hashtags = ArrayField(models.CharField(max_length=255, null=True, blank=True), null=True, blank=True)

    def __str__(self):
        return f'%s for %s' % (self.sender, self.post)


class Like(models.Model, SuperModel):
    """Post like model

    Properties:
        - liker (Blogger): Instance of Blogger model
        - post (Post): Instance of Post model
    """
    liker = models.ForeignKey(to=Blogger, on_delete=models.CASCADE)
    post = models.ForeignKey(to=Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('liker', 'post')

    def __str__(self):
        return f'%s like %s' % (self.liker, self.post)
