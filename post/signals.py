import re
from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch.dispatcher import receiver
from post.models import Like, Post, PostSetting, Comment
from blog import settings


@receiver(pre_save, sender=Post)
def add_mentions_hashtashs(sender, instance, **kwargs):
    try:
        hashtags = re.findall(r'#\w+', instance.description + ' ' + instance.content)
        mentions = re.findall(r'@\w+', instance.description + ' ' + instance.content)
        instance.mentions = mentions
        instance.hashtags = hashtags

    except Exception as e:
        if settings.DEBUG:
            print(e)
        instance.delete()


@receiver(post_save, sender=Post)
def add_mentions_hashtashs(sender, instance, created, **kwargs):
    if created:
        try:
            PostSetting.objects.create(post=instance)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            instance.delete()


@receiver(post_save, sender=Like)
def increment_likes(sender, instance, created, **kwargs):
    if created:
        try:

            post = instance.post
            blogger = instance.post.blogger

            post.likes_count += 1
            blogger.likes_count += 1

            post.save()
            blogger.save()

        except Exception as e:
            if settings.DEBUG:
                print(e)

            instance.delete()


@receiver(post_delete, sender=Like)
def decrement_likes(sender, instance, **kwargs):
    try:
        post = instance.post
        blogger = instance.post.blogger

        post.like_count -= 1
        blogger.likes_count -= 1

        post.save()
        blogger.save()

    except Exception as e:
        if settings.DEBUG:
            print(e)


@receiver(pre_save, sender=Comment)
def add_comment_mentions_hashtashs(sender, instance, **kwargs):
    try:
        hashtags = re.findall(r'#\w+', instance.text)
        mentions = re.findall(r'@\w+', instance.text)
        instance.mentions = mentions
        instance.hashtags = hashtags

    except Exception as e:
        if settings.DEBUG:
            print(e)
        instance.delete()


@receiver(post_save, sender=Comment)
def increment_comment(sender, instance, created, **kwargs):
    if created:
        try:

            post = instance.post
            blogger = instance.post.blogger

            post.comments_count += 1
            blogger.comments_count += 1

            post.save()
            blogger.save()

        except Exception as e:
            if settings.DEBUG:
                print(e)

            instance.delete()


@receiver(post_delete, sender=Comment)
def decrement_comment(sender, instance, **kwargs):
    try:
        post = instance.post
        blogger = instance.post.blogger

        post.comments_count -= 1
        blogger.comments_count -= 1

        post.save()
        blogger.save()

    except Exception as e:
        if settings.DEBUG:
            print(e)
