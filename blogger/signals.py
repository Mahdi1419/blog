from django.db.models.signals import post_save, pre_delete, post_delete, pre_save
from django.dispatch import receiver

from account.models import Account
from .models import Blogger, BloggerSetting, Follow
from blog import settings


@receiver(post_save, sender=Account)
def create_blogger(sender, instance, created, **kwargs):
    if created:
        """create blogger after new account created"""
        try:
            Blogger.objects.create(account=instance, name=instance.name, family=instance.family, age=instance.age)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            instance.delete()


@receiver(post_save, sender=Blogger)
def create_blogger_setting(sender, instance, created, **kwargs):
    if created:
        try:
            """create blogger setting after blogger created"""
            BloggerSetting.objects.create(blogger=instance)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            instance.delete()


@receiver(post_save, sender=Follow)
def handle_follow(sender, instance, created, **kwargs):
    if created:
        try:
            """set follow status (pending=0 or accepted=1)"""
            follower = instance.follower
            following = instance.following

            if not following.bloggersetting.privacy:
                status = 1
                follower.followings_count += 1
                following.followers_count += 1
                follower.save()
                following.save()
            else:
                status = 0

            instance.status = status
            instance.save()

        except Exception as e:
            if settings.DEBUG:
                print(e)
            instance.delete()


@receiver(pre_delete, sender=Follow)
def delete_follow(sender, instance, **kwargs):
    try:
        """delete follow object"""

        follower = instance.follower
        following = instance.following

        if instance.status is 1:
            print('here')
            follower.followings_count -= 1
            following.followers_count -= 1
            follower.save()
            following.save()

    except Exception as e:
        if settings.DEBUG:
            print(e)
