
from django.urls import path, include
from .views import BloggerView, SingleBloggerView, SingleBloggerPostView, BloggerSettingsView, BloggerFollowView
urlpatterns = [
    path('', BloggerView.as_view(), name='blogger'),
    path('settings/', BloggerSettingsView.as_view(), name='blogger_setting'),
    path('<str:username>', SingleBloggerView.as_view(), name='detail'),
    path('<str:username>/posts/', SingleBloggerPostView.as_view(), name='blogger_posts'),
    path('<str:username>/follow/', BloggerFollowView.as_view(), name='follow'),
]