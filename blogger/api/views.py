from django.views.generic.base import View
from account.api.decorators import authRequired, activeRequired
from account.api.validators import VerifyCodeValidator
from account.models import VerifyCode
from api_app.decorators import parmsCheck
from api_app.utils import getUnixTime
from api_app.validator.validator import ValidateData, ValidationError
from api_app.validator.fields import IntegerField
from post.api.serializers import PostSerializer
from post.models import Post
from .blogger_manager import BloggerManager
from .response import BloggerStaticResponse
from .serializers import BloggerSerializer
from .validators import BloggerValidator, SettingValidator
from ..models import Blogger, Follow
from api_app.response import ApiResponse, StaticResponse
from blog import settings


class BloggerView(View):
    """Get blogger data from database"""

    @authRequired
    @activeRequired
    def get(self, request):

        try:
            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # serialize blogger data
            serialize_data = BloggerSerializer(instance=auth_blogger)

            # return blogger serialize data
            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

    @authRequired
    @activeRequired
    @parmsCheck(needed_params=['username', 'bio', 'gender', 'name', 'family', 'age'])
    def post(self, request):
        try:

            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # check username
            if auth_blogger.username != request.data['username']:
                blogger = BloggerManager.get_blogger_with_username(request.data['username'])
                if blogger:
                    return ApiResponse({"message": "Username Already exist"}, status=403)

            # validate data
            validate: ValidateData = BloggerValidator(data=request.data)
            if not validate.is_valid():
                return ApiResponse(validate.get_messages(), status=400)

            update = auth_blogger.custom_update(data=validate.get_validate_data(), update_fields=[
                "name", "family", "username", "age", "gender", "bio"
            ])

            if not update:
                return ApiResponse("update failed", status=500)

            # get blogger data
            serialize_data = BloggerSerializer(instance=update, separate=['settings'])

            # return serialize data
            return ApiResponse(serialize_data, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

    @authRequired
    @activeRequired
    @parmsCheck(needed_params=['code'])
    def delete(self, request):
        try:
            # get blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            def checkDigits(value):
                if value not in range(10000, 99999):
                    raise ValidationError("code must be 5 digit")

            validate: ValidateData = VerifyCodeValidator(data=request.data, fields=[
                ('code', IntegerField(validators=[checkDigits]))
            ])
            if not validate.is_valid():
                return ApiResponse(validate.get_messages(), status=400)

            # get verify code
            verify_obj = VerifyCode.is_exist({
                "phone": auth_blogger.account.phone,
                "code_for": "delete"
            })

            if not verify_obj:
                return StaticResponse.PhoneNotVerify

            # check code
            if verify_obj.code != validate.get_validate_data().get('code'):
                return StaticResponse.VerifyCodeValidationError

            # check code expire time
            if getUnixTime() > verify_obj.expire_time:
                verify_obj.delete()
                return StaticResponse.VerifyCodeIsExpire

            auth_blogger.account.is_active = False
            auth_blogger.account.save()

            return ApiResponse({"message": "Account deleted successfully"}, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError


class SingleBloggerView(View):
    # data transforms
    def add_setting(obj, instance):
        return {
            "settings": {
                "privacy": obj.settings.get('privacy'),
                "comments": obj.settings.get('comments')
            }
        }

    def get(self, request, username):
        try:
            blogger_obj = Blogger.is_exist({
                "username": username
            })
            if not blogger_obj:
                return ApiResponse({"message": "username accessible"}, status=200)

            return ApiResponse({"message": "username not accessible"}, status=403)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

    def post(self, request, username):
        try:
            # get blogger
            blogger = Blogger.is_exist({
                "username": username
            })

            # check blogger existence
            if not blogger or not blogger.account.is_active:
                return ApiResponse({"message": "account not found"})

            # get blogger data
            serialize_data = BloggerSerializer(instance=blogger, transform_functions=[self.add_setting])

            # return serialize data
            return ApiResponse(serialize_data)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError


class BloggerSettingsView(View):
    """get, edit Blogger settings"""

    @authRequired
    @activeRequired
    @parmsCheck(needed_params=['privacy', 'comments'])
    def post(self, request):
        try:
            # get auth blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # validate data
            validate: ValidateData = SettingValidator(data=request.data)

            # check validator
            if not validate.is_valid():
                return ApiResponse(validate.get_messages(), status=400)

            # get blogger settings
            auth_blogger_setting = auth_blogger.bloggersetting

            # update settings
            update = auth_blogger_setting.custom_update(validate.get_validate_data(),
                                                        update_fields=['privacy', 'comments'])

            if not update:
                return ApiResponse({"message": "Update failed"}, status=500)

            return ApiResponse({"messages": "Update was successfully"})

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError


class SingleBloggerPostView(View):
    """get bloggers posts"""

    def get(self, request, username):
        try:
            blogger = BloggerManager.get_blogger_with_username(username)
            if blogger is None or not blogger.account.is_active:
                return ApiResponse({"message": "Account dose not exist"})

            # get posts
            posts = Post.is_exist({"blogger": blogger}, many=True)

            # serialize posts data
            serialize_data = PostSerializer(instance=posts, many=True, separate=['pin', 'archive', 'updated_at'])

            # check page privacy
            if blogger.bloggersetting.privacy:
                # check authentication
                if request.auth_user is not None:

                    # get auth blogger
                    auth_blogger = Blogger.objects.get(account=request.auth_user)

                    # search in blogger followers
                    follow_obj = BloggerManager.check_follow(follower=auth_blogger, following=blogger, accepted=1)
                    if not follow_obj:
                        return BloggerStaticResponse.PageIsPrivate
                    return ApiResponse(serialize_data)
                else:
                    return BloggerStaticResponse.PageIsPrivate

            return ApiResponse(serialize_data)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

    def post(self, request, username):
        try:
            pass
        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError


class BloggerFollowView(View):
    """follow and unfollow bloggers"""

    @authRequired
    @activeRequired
    def post(self, request, username):
        """follow blogger"""
        try:

            # get auth_blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get blogger
            blogger = BloggerManager.get_blogger_with_username(username)
            if blogger is None or not blogger.account.is_active:
                return ApiResponse({"message": "Account not fount"}, status=404)

            follow = BloggerManager.check_follow(auth_blogger, blogger)
            if follow:
                return ApiResponse({"message": "you already follow the blogger"}, status=409)

            # create new follow object
            follow_obj = Follow(
                follower=auth_blogger,
                following=blogger
            )
            follow_obj.save()

            return ApiResponse({"message": "follow successfully"}, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

    @authRequired
    @activeRequired
    def delete(self, request, username):
        """unfollow blogger"""
        try:
            # get auth_blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get blogger
            blogger = BloggerManager.get_blogger_with_username(username)
            if blogger is None or not blogger.account.is_active:
                return ApiResponse({"message": "Account not fount"}, status=404)

            follow = BloggerManager.check_follow(auth_blogger, blogger)
            if not follow:
                return ApiResponse({"message": "You not followed the blogger yet "}, status=404)

            follow.delete()
            return ApiResponse({"message": "unfollow successfully"}, status=200)

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError

    def put(self, request, username):
        """accept follow"""
        try:
            # get auth_blogger
            auth_blogger = Blogger.objects.get(account=request.auth_user)

            # get blogger
            blogger = BloggerManager.get_blogger_with_username(username)
            if blogger is None or not blogger.account.is_active:
                return ApiResponse({"message": "Account not fount"})

            follow = BloggerManager.check_follow(blogger, auth_blogger)
            if not follow:
                return ApiResponse({"message": "Blogger not followed you"}, status=404)

            if follow.status == 1:
                return ApiResponse({"message": "You accept the blogger"}, status=202)

            follower = follow.follower
            following = follow.following

            follower.followings_count += 1
            following.followers_count += 1

            follower.save()
            following.save()

            follow.status = 1
            follow.save()

            return ApiResponse({"message": 'Blogger accept successfully'})
        except Exception as e:
            if settings.DEBUG:
                print(e)
            return StaticResponse.ServerError
