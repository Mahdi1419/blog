# from rest_framework import serializers
from api_app.Serializer import Serializer
from ..models import Blogger


class BloggerSerializer(Serializer):
    
    account = Serializer.RelationField(fields=['name', 'family', 'id'], field_name='info')
    bloggersetting = Serializer.RelationField(except_fields=['id', 'blogger_id'], field_name='settings')
    fullname = Serializer.CustomField('get_fullname')
    username = Serializer.CustomField('get_username')

    class Meta:
        model = Blogger
        fields = '__all__'
        except_fields = ['info', 'id']

    def get_username(self, obj, instance, serializer):
        """add at_sign to username"""
        return '@%s' % obj.username

    def get_fullname(self, obj, instance, serializer):
        """get blogger full name"""
        return '%s %s' % (obj.info.get('name'), obj.info.get('family'))
    
    
    