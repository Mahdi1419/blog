from blogger.models import Follow, Blogger
from typing import Union
from blog import settings


class BloggerManager:
    classmethod

    def get_blogger_with_username(username: str):
        try:
            blogger = Blogger.is_exist({
                "username": username
            })

            if not Blogger or not blogger.account.is_active:
                return None

            return blogger

        except Exception as e:
            if settings.DEBUG:
                print(e)
            return None

    classmethod

    def check_follow(follower: Blogger, following: Blogger, accepted: int = None) -> Union[bool, Follow]:
        """Check follow

        Args:
            follower (Blogger)
            following (Blogger)
            accepted (bool)

        Returns: [Follow ,bool]

        """

        follow_obj = Follow.is_exist({
            "follower": follower,
            "following": following,
        })

        if accepted is None:
            if not follow_obj:
                False

            return follow_obj

        if follow_obj.status is accepted:
            return follow_obj

        return False
