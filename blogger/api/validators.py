from api_app.validator.validator import Validator
from api_app.validator.fields import *
from .blogger_manager import BloggerManager


class BloggerValidator(Validator):
    class Meta:
        # def username_check(value):
        #     blogger = BloggerManager.get_blogger_with_username(value)
        #     if blogger is not None:
        #         raise ValidationError("Username Already exist")

        gender = (
            ('M', 'Male'),
            ('F', 'Female'),
            ('O', 'Other'),
        )

        fields = [
            ('username', CharField(min_length=5, max_length=20)),
            ('name', CharField(min_length=2, max_length=50)),
            ('family', CharField(min_length=2, max_length=60)),
            ('gender', ChoiceField(choices=gender)),
            ('bio', CharField(max_length=120)),
            ('age', IntegerField(min_value=18, max_value=150)),
        ]


class SettingValidator(Validator):
    class Meta:
        fields = [
            ('comments', BooleanField()),
            ('privacy', BooleanField())
        ]
