from api_app.response import ApiResponse


class BloggerStaticResponse:
    """
    static json responses
    """

    PageIsPrivate = ApiResponse({
        "message": 'page is private'
    }, status=500)


