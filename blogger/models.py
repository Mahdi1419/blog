import uuid

# models
from django.db import models
from account.models import Account
from api_app.utils import SuperModel

# Create your models here.


class Blogger(models.Model, SuperModel):
    """Blogger Model

    Properties:
        - account (Account): Instance of Account model
        - username (str)
        - profile (str): Must be a url
        - bio (str)
        - gender (str): 'M' for male, 'F' for female and 'O' for other
    """
    uid = models.UUIDField(default=uuid.uuid4)
    account = models.OneToOneField(Account, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, blank=False, null=False)
    name = models.CharField(max_length=50, blank=False, null=False)
    family = models.CharField(max_length=100, blank=False, null=False)
    age = models.IntegerField()
    profile = models.URLField(blank=True, null=True)
    bio = models.CharField(max_length=510, null=True, blank=True)
    gender = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

    gender = models.CharField(max_length=1, choices=gender, null=True, blank=True)
    
    post_count = models.IntegerField(default=0)
    followers_count = models.IntegerField(default=0)
    followings_count = models.IntegerField(default=0)
    views_count = models.IntegerField(default=0)
    likes_count = models.IntegerField(default=0)
    comments_count = models.IntegerField(default=0)
    
    def __str__(self):
        return "@%s" % self.username
    

class BloggerSetting(models.Model, SuperModel):
    """Blogger Setting
    
    Properties:
        - blogger (Blogger): Instance of Blogger model
        - privacy (boolean): Public or private blogger page
        - comments (boolean): Closed or open comment of all blog posts
    """
    uid = models.UUIDField(default=uuid.uuid4)
    blogger = models.OneToOneField(Blogger, on_delete=models.CASCADE)
    privacy = models.BooleanField(default=False)
    comments = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.blogger}'


class Follow(models.Model, SuperModel):
    """Blogee Follow Model
    
    Properties:
        - follower (Blogger): Instance of Blogger
        - following (Blogger): Instance of Blogger
        - status (str): 'pending' or 'accepted'
    """
    uid = models.UUIDField(default=uuid.uuid4)
    follower = models.ForeignKey(Blogger, related_name='follower', on_delete=models.CASCADE)
    following = models.ForeignKey(Blogger, related_name='following', on_delete=models.CASCADE)
    follow_at = models.DateTimeField(auto_now_add=True)

    status = models.IntegerField(default=0)

    class Meta:
        unique_together = ('follower', 'following')

    def __str__(self):
        return f'%s follow %s' % (self.follower, self.following)