from django.contrib import admin

# models
from .models import Blogger, BloggerSetting, Follow
# Register your models here.

admin.site.register((Blogger, BloggerSetting, Follow))